import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Global } from '../../services/global';
import { ArticleService } from '../../services/article.service';
import { Article } from '../../models/article';
import swal from 'sweetalert';

@Component({
  selector: 'app-article-edit',
  templateUrl: '../article-new/article-new.component.html',
  styleUrls: ['./article-edit.component.css'],
  providers: [ArticleService]
})
export class ArticleEditComponent implements OnInit {

  public article: Article
  public status: string
  public is_edit: boolean
  public page_title: string
  public url: string

  afuConfig = {
    multiple: false,
    formatsAllowed: ".jpg,.png, .gif, .jpeg",
    maxSize: "50",
    uploadAPI:  {
      url: Global.url+'upload-image'
    },
    theme: "attachPin",
    hideProgressBar: true,
    hideResetBtn: true,
    hideSelectBtn: false,
    fileNameIndex: true,
    replaceTexts: {
      selectFileBtn: 'Select Files',
      resetBtn: 'Reset',
      uploadBtn: 'Upload',
      dragNDropBox: 'Drag N Drop',
      attachPinBtn: 'Attach an image...',
      afterUploadMsg_success: 'Successfully Uploaded !',
      afterUploadMsg_error: 'Upload Failed !',
      sizeLimit: 'Size Limit'
    }
  }

  constructor(

    private _articleService: ArticleService,
    private _route: ActivatedRoute,
    private _router: Router
  )
    {
      this.article = new Article('', '', '', null, null)
      this.is_edit = true
      this.page_title = 'Edit article'
      this.url = Global.url
     }

  ngOnInit(): void {
    this.getArticle()
  }

  onSubmit() {
    this._articleService.update(this.article._id, this.article).subscribe(
      response => {
        if(response.status == 'success') {
          this.status = response.status
          this.article = response.article
          // Alert
          swal(
            'Article has been created',
            'Article has been updated',
            'success'
          )
          this._router.navigate(['/blog'])
        } else {
          this.status = 'error'
        }
      },
      error => {
        console.log(error)
        this.status = 'error'
        swal(
          'Error during editing',
          'Article could not be edited',
          'error'
        )
      }
    )
  }

  imageUpload(data) {
    let image_data =  JSON.parse(JSON.stringify(data.body.image))
    this.article.image = image_data
  }

  getArticle() {
    this._route.params.subscribe(params => {
      let id = params['id']
      this._articleService.getArticle(id).subscribe(
        response => {
          if(response.article) {
            this.article = response.article
          } else {
            this._router.navigate(['/home'])
          }
        },
        error => {
          this._router.navigate(['/home'])
        }
      )
    })
  }
}
