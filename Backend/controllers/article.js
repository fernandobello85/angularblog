'use strict'
var validator = require('validator')
var fs = require('fs')
var path = require('path')
var Article = require('../models/article')

var controller = {
  // datos: (req, res) => {
  //   var saludo = req.body.hola
  //   return res.status(200).send({
  //     curso: 'Master Frameworks JS',
  //     url: 'Udemy2',
  //     saludo
  //   })
  // },

  // test: (req, res) => {
  //   return res.status(200).send({
  //     message: 'Soy la acction test de mi controller',
  //   })
  // },

  save: (req, res) => {
      // Collect post parameters
      var params = req.body
      // Validate data
      try{
        var validate_title = !validator.isEmpty(params.title)
        var validate_content = ! validator.isEmpty(params.content)
      }catch(err){
        return res.status(200).send({
          status: 'error',
          message: 'Data missing',
        })
      }
      if(validate_title && validate_content){
        // Create object to save
        var article = new Article()
        // Assign values
        article.title = params.title
        article.content = params.content
        if (params.image) {
          article.image = params.image
        } else {
          article.image = null
        }
        // Save artcile
        article.save((err, articleStored) => {
          if(err || !articleStored) {
            return res.status(404).send({
              status: 'error',
              message: 'Unexpected error saving article'
            })
          } else {
            // Return response
            return res.status(200).send({
              status: 'success',
              articleStored
            })
          }
        })
      } else {
        return res.status(200).send({
          status: 'error',
          message: 'Invalid data',
        })
      }
  },

  getArticles: (req, res) => {
    // Find articles
    var query = Article.find({})
    var last = req.params.last
    if (last || last != undefined) {
      query.limit(5)
    }
    query.sort('-_id').exec((err, articles) => {
      if(err) {
        return res.status(500).send({
          status: 'error',
          message: 'Unexpected error',
        })
      }
      if(!articles) {
        return res.status(404).send({
          status: 'error',
          message: 'Unable to find articles',
        })
      }
      return res.status(200).send({
        status: 'success',
        articles
      })
    })
  },

  getArticle: (req, res) => {
    // Collect article Id
    var articleId = req.params.id
    // Validate exists
    if (articleId == null || !articleId) {
      return res.status(404).send({
        status: 'error',
        message: 'Error on sended id',
      })
    }
    // Find article
    Article.findById(articleId, (err, article) => {
      if(err || !article) {
        return res.status(404).send({
          status: 'error',
          message: 'Article does not exist',
        })
      }
      return res.status(200).send({
        status: 'success',
        article
      })
    })
  },

  update: (req, res) => {
    // Collect article Id and params
    var articleId = req.params.id
    var params = req.body
    console.log(params)
    // Validate data
    try{
      var validate_title = !validator.isEmpty(params.title)
      var validate_content = !validator.isEmpty(params.content)
    }catch(err){
      return res.status(404).send({
        status: 'error',
        message: 'Missing data',
      })
    }
    if(validate_title && validate_content){
      Article.findOneAndUpdate({_id: articleId}, params, {new:true}, (err, articleUpdated) => {
        if(err) {
          return res.status(500).send({
            status: 'error',
            message: 'Error updating article',
          })
        }
        if(!articleUpdated) {
          return res.status(404).send({
            status: 'error',
            message: 'Article does not exist',
          })
        }
        return res.status(200).send({
          status: 'success',
          article: articleUpdated
        })
      })
    } else {
      return res.status(404).send({
        status: 'error',
        message: 'Invalid data',
      })
    }
  },

  delete: (req, res) => {
    var articleId = req.params.id
    Article.findOneAndDelete({_id: articleId}, (err, articleRemoved) => {
      if(err) {
        return res.status(500).send({
          status: 'error',
          message: 'Error removing article',
        })
      }
        if(!articleRemoved) {
          return res.status(404).send({
            status: 'error',
            message: 'Unable to remove article',
          })
      }
      return res.status(200).send({
        status: 'success',
        article: articleRemoved
      })
    })
  },

  upload: (req, res) => {
    // Collet request file
    var file_name = 'Imagen no cargada...'
    // Validate file extension
    if(!req.files) {
      return res.status(404).send({
        status: 'error',
        message: 'Unable to load file',
      })
    }
    // Take name and extension
    var file_path = req.files.file0.path
    var file_split = file_path.split('/')
    // File name
    var file_name = file_split[2]
    // Extenion file
    var extension_split = file_name.split('.')
    var extension_file = extension_split[1]
    // If validated
    if(extension_file != 'png' && extension_file != 'jpg' && extension_file != 'jpeg' && extension_file != 'gif') {
      // Remove file
      fs.unlink(file_path, (err) => {
        return res.status(200).send({
          status: 'error',
          message: 'File extension is not valid',
        })
      })
    } else {
      var articleId = req.params.id
      if (articleId) {
        // Find article, assign name and update
        Article.findOneAndUpdate({_id: articleId}, {image: file_name}, {new: true}, (err, articleUpdated) =>  {
          if(err || !articleUpdated) {
            return res.status(500).send({
              status: 'error',
              message: 'Error loading image'
            })
          }
          return res.status(200).send({
            status: 'success',
            article: articleUpdated
          })
        })
      } else {
        return res.status(200).send({
          status: 'success',
          image: file_name
        })
      }
    }
  },

  getImage: (req, res) => {
    var file = req.params.image
    var path_file = 'upload/articles/'+file
    fs.exists(path_file, (exists) => {
      if(exists) {
        return res.sendFile(path.resolve(path_file))
      } else {
        return res.status(500).send({
          status: 'error',
          message: 'Image does not exist'
        })
      }
    })
  },

  search: (req, res) => {
    var searchString = req.params.search
    Article.find({ "$or": [
      { "title": { "$regex": searchString, "$options": "i"}},
      { "content": { "$regex": searchString, "$options": "i"}}
    ]})
    .sort([['date', 'descending']])
    .exec((err, articles) => {
      if(err) {
        return res.status(500).send({
          status: 'error',
          message: 'Query error'
        })
      }
      if(!articles || articles.length <= 0) {
        return res.status(404).send({
          status: 'error',
          message: 'No restuls for this query'
        })
      }
      return res.status(200).send({
        status: 'success',
        articles
      })
    })
  }
}

module.exports = controller
