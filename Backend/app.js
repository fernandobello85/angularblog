'use strict'

// Load node modules to create server
var express = require('express')
var bodyParser = require('body-parser')
// Exec express (http)
var app = express()
// Load routes files
var article_routes = require('./routes/article')

// Middlewares
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

// CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});
// Add routes prefix / Load routes
app.use('/api', article_routes)
// Export module (actual file)
module.exports = app
